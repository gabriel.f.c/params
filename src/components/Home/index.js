import { Link } from 'react-router-dom'
import { members } from '../Membros'
import './style.css'

function Home() {
    return (
        <div className="home">
            {members.map(x => x.type === `pj` ? 
            <Link to={`/company/${x.id}`} >{x.name}</Link> 
            : 
            <Link to={`/customer/${x.id}`}>{x.name}</Link>)}
        </div>
    )
}

export default Home