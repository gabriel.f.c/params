import { Route, useHistory } from 'react-router-dom'
import Company from "./pages/Company"
import Customer from "./pages/Customer"
import Home from './components/Home';
import './components/Membros/index'
import './App.css';

function App() {


  return (
    <div className="App">
      <header className="App-header">
        <Route path="/customer/:id">
          <Customer/>
        </Route>
        <Route path="/company/:id">
          <Company/>
        </Route>
        <Route exact path="/">
          <Home/>
        </Route>
      </header>
    </div>
  );
}

export default App;
