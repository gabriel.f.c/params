import { useParams, Link } from 'react-router-dom'
import { members } from '../../components/Membros'

function Company() {

    const params = useParams()
    const member = members.filter(x => x.id === params.id)

    return (
        <div>
            <h1>Detalhes da Empresa</h1>

            <p>
                Nome da empresa: {member[0].name}
            </p>

            <Link to="/">
                Voltar
            </Link>
        </div>
    )

}

export default Company