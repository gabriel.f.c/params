import { useParams, Link } from 'react-router-dom'
import { members } from '../../components/Membros'

function Customer() {

    const params = useParams()

    const member = members.filter(x => x.id === params.id)

    return (
        <div>
            <div>
                <h1>Detalhes do cliente</h1>

                <p>
                    Nome: {member[0].name}
                </p>

                <Link to="/">
                    Voltar
                </Link>
            </div>
        </div>
    )

}

export default Customer